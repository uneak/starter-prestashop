# Uneak prestashop starter

#### Prestashop e-commerce

Images Docker utilisées [Nginx, Prestashop-fpm, MySQL and PHPMyAdmin](#images-docker-utilisées).

## Sommaire

1. [Installer les prérequis](#installer-les-prérequis)

Avant d'installer le projet, assurez-vous que les conditions préalables suivantes sont remplies.

2. [Cloner le projet](#cloner-le-projet)

Nous allons télécharger le code depuis son repository.

3. [Paramétrer le projet et les containers](#paramétrer-le-projet-et-les-containers)

Nous allons paramétrer le projet et le serveur.

4. [Générer le certificat SSL](#générer-le-certificat-ssl)

Nous allons générer le certificat SSL pour `Nginx` avant d'exécuter le serveur.

5. [Lancer l'application](#lancer-l'application)

À ce stade, nous aurons tous les éléments du projet en place.

6. [Utiliser Makefile](#utiliser-makefile) _[optionnel]_

Lors du développement, vous pouvez utiliser `Makefile` pour effectuer des opérations récurrentes.

7. [Utiliser les commandes Docker](#utiliser-les-commandes-docker)

Lors de l'exécution, vous pouvez utiliser les commandes docker pour effectuer des opérations récurrentes.

---

## Installer les prérequis

Pour l'instant, ce projet a été principalement créé pour Unix `(Linux/MacOS)`. Il n'a pas encore été testé sur Windows.

Tous les éléments requis doivent être disponibles pour le projet.

Les plus importants sont :

- [Git](https://git-scm.com/downloads)
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker-Compose](https://docs.docker.com/compose/install/)

> Pour **Docker** et **Docker-compose**, le plus simple est d'installer l'application Desktop.
> *Avec [Docker Desktop](https://docs.docker.com/compose/install/compose-desktop/), vous obtenez Docker Engine, Docker CLI avec le plugin Compose ainsi que d'autres composants et outils.*

Vérifiez si `docker-compose` est déjà installé en saisissant la commande suivante :

```sh
which docker-compose
```

Vérifier la compatibilité Docker-Compose :

- [Références Compose file version 3](https://docs.docker.com/compose/compose-file/)

Ce qui suit est facultatif mais rend la vie plus agréable :

```sh
which make
```

Sur Ubuntu et Debian, ils sont disponibles dans le package build-essential. Sur d'autres distributions, vous devrez peut-être installer le compilateur GNU C++ séparément.

```sh
sudo apt install build-essential
```

Sur Mac, il vous suffit de l'installer via xcode *(à vérifier)*

```sh
xcode-select --install
```

où via [brew](https://brew.sh/index_fr)

```sh
brew install make
```

### Images Docker utilisées

- [Nginx](https://hub.docker.com/_/nginx/)
- [MySQL](https://hub.docker.com/_/mysql/)
- [Prestashop-fpm](https://hub.docker.com/r/prestashop/prestashop/)
- [Composer](https://hub.docker.com/_/composer/)
- [PHPMyAdmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin/)
- [Generate Certificate](https://hub.docker.com/r/jacoelho/generate-certificate/)

Les ports sont entièrement configurables par le fichier `.env` mais la configuration la plus logique serait la suivante:


| Serveur    | Port |
| ------------ | ------ |
| MySQL      | 3306 |
| PHPMyAdmin | 8080 |
| Nginx      | 80   |
| Nginx SSL  | 443  |

---

## Cloner le projet

Installez le projet en suivant les instructions :

```sh
git clone git@gitlab.com:uneak/starter-prestashop.git
```

Allez dans le répertoire du projet :

```sh
cd starter-prestashop
```

### Arborescence du projet

```sh
.
├── config
│   ├── nginx
│   │   └── default.template.conf
│   └── php
│       ├── php.dev.ini
│       ├── php.prod.ini
│       └── php.ini
├── data
│   ├── db
│   │   └── [database].sql
│   ├── nginx
│   │   ├── conf.d
│   │   │   └── default.conf
│   │   └── log
│   │       └── ...
│   └── ssl
│       └── [domaine]
│           ├── server.key
│           └── server.pem
├── public
│   └── [fichiers prestashop]
└── .env
```

- `config`: Configurations utilisées pour construire les containers `Nginx` et `Prestashop`
- `data`: Données générées

  - `db`: Utilisé pour dumper et/où restaurer la base de donnée
  - `nginx`: Configuration actuelle du vhost `Nginx`
  - `ssl`: Certificat SSL du vhost `Nginx`
- `public`: Sources du site
- `.env`: Fichier de paramétrage du projet

---

## Paramétrer le projet et les containers

Renommez le fichier de config `.env.template` en `.env`

```sh
mv .env.template .env
```

### Editer le fichier de config

Le fichier de configuration `.env` est requis et doit être configuré pour le projet.
Les paramètres les plus importants sont :

- `PROJECT_NAME`: Nom du projet
- `DOMAIN`: Nom de domaine

###### Nginx *(serveur web)*

- `NGINX_DOCKER_VERSION`: Version du serveur `Nginx`
- `NGINX_DOCKER_HTTP_PORT_PUBLISHED`: Port HTTP sur lequel le serveur `Nginx` sera publié
- `NGINX_DOCKER_HTTPS_PORT_PUBLISHED`: Port HTTPS sur lequel le serveur `Nginx` sera publié

###### MySql *(serveur de base de donnée)*

- `MYSQL_DOCKER_VERSION`: Version du serveur `MySql`
- `MYSQL_DOCKER_HTTP_PORT_PUBLISHED`: Port HTTP sur lequel le serveur `MySql` sera publié
- `MYSQL_ROOT_PASSWORD`: Mot de passe de l'utilisateur **root**
- `MYSQL_ALLOW_EMPTY_PASSWORD`: Autorise un mot de passe vide
- `MYSQL_DATABASE`: Nom de la base de données
- `MYSQL_USER`: Nom d'utilisateur de la base de données
- `MYSQL_PASSWORD`: Mot de passe de la base de données

###### Prestashop *([Service PHP-fpm](https://www.php.net/manual/fr/install.fpm.php))*

- `PS_DOCKER_VERSION`: Version de prestashop
- `PS_PHP_DOCKER_VERSION`: Version de PHP
- `PS_HOST_MODE`: Simule un environnement PrestaShop Cloud
- `PS_DEMO_MODE`: Crée une boutique de démonstration
- `PS_INSTALL_AUTO`: Exécution de l'installation automatiquement
- `PS_LANGUAGE`: Langue par défaut installée avec PrestaShop
- `PS_COUNTRY`: Pays par défaut installé avec PrestaShop
- `PS_ALL_LANGUAGES`: Installe toutes les langues existantes pour la version actuelle
- `PS_FOLDER_ADMIN`: Nom du dossier d'administration du site
- `PS_FOLDER_INSTALL`: Nom du dossier d'installation du site
- `PS_ENABLE_SSL`: Active le SSL
- `ADMIN_MAIL`: E-mail de l'administrateur
- `ADMIN_PASSWD`: Mot de passe de l'administrateur
- `DB_PREFIX`: Préfixe par défaut des tables

###### PhpMyAdmin *(uniquement en environement de dev)*

- `PMA_DOCKER_VERSION`: Version du serveur PhpMyAdmin
- `PMA_DOCKER_HTTP_PORT_PUBLISHED`: Port HTTP sur lequel le serveur PhpMyAdmin sera publié

## Générer le certificat SSL

```sh
source .env && sudo docker run --rm -v $(shell pwd)/$(SSL_DIR)/$(DOMAIN):/certificates -e "SERVER=$(DOMAIN)" -e "SUBJECT=/C=CA/ST=Canada/L=Canada/O=IT" jacoelho/generate-certificate
```

> *Il est possible de générer le certificat plus facilement avec [une commande `Makefile`](#utiliser-makefile)*

---

## Lancer l'application

Pour démarrer l'application vous pouvez utiliser la commande Docker-compose suivante:

```sh
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d # pour l'environement de développement
```

où

```sh
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d # pour l'environement de production
```

**Veuillez patienter, cela peut prendre plusieurs minutes......**

Vous pouvez voir les logs en utilisant la commande précédente sans l'option `-d` ou avec la commande:

```sh
docker-compose logs -f
```

1. Ouvrez votre navigateur :

   - [http://localhost](http://localhost/) *HTTP*
   - [https://localhost](https://localhost/) *HTTPS*
   - [http://localhost:8080](http://localhost:8080/) *PHPMyAdmin*

> *Ajoutez à l'url `:[PORT]` si vous avez configuré un port spécial (ex: http://localhost:8386)*

2. Arrêtez et nettoyez les services docker avec la commande:

```sh
docker-compose down --remove-orphans
```

---

## Utiliser Makefile

Lors du développement, vous pouvez utiliser [Makefile](https://en.wikipedia.org/wiki/Make_(software)) pour effectuer les opérations suivantes :


| Nom        | Description                                                        |
| ------------ | -------------------------------------------------------------------- |
| install    | Crée le projet.*(à utiliser uniquement à l'initialisation)*     |
| reset      | Supprime tous les fichiers du projet.*(à utiliser avec prudence)* |
| clean      | Supprime les informations relatives au projet.*(dossier`data`)*    |
| build      | Crée les containers                                               |
| start      | Crée et démarre les services                                     |
| stop       | Arrête et nettoie les services                                    |
| ssl        | Génère le certificat SSL                                         |
| dump-db    | Crée un backup de la base de données du projet                   |
| restore-db | Restaure la base de données du projet                             |

> *Pour les commandes concernant les containers, vous pouvez changer l'environement de travail en ajoutant la variable `env` avec la valeur `prod` où `dev` à la suite de la commande*

> *Lors du dump/restore de la base de données, vous pouvez sélectionner une base de données et un fichier d'entrée/sortie en ajoutant les variables `db` et `file` à la suite de la commande*

### Exemples

Installer l'application:
*Nettoie entièrement le projet et génère le certificat SSL avant de créer tous les services*

```sh
make install
```

Backup et restauration de la base de données:

```sh
# dump de toutes les bases de données
make dump-db db=--all-databases file=all_db.sql

# dump/restore de la base de données du projet
make dump-db file=db-v2.sql
make restore-db file=db-v2.sql

# dump/restore de la base de données 'presta_copy'
make dump-db file=presta_copy-v2.sql
make restore-db db=presta_copy file=presta_copy-v2.sql
```

Construire les containers de production docker:

```sh
make build env=prod
```

Afficher l'aide:

```sh
make help
```

---

## Utiliser les commandes Docker

### Installation des packages avec composer

```sh
docker-compose exec prestashop composer require symfony/yaml
```

### Mise à jour des dépendances avec composer

```sh
docker-compose exec prestashop composer update
```

### Utilisation de la console prestashop

```sh
docker-compose exec prestashop bin/console
```

### Vérification des extensions PHP installées

```sh
docker-compose exec php php -m
```

### Gestion de la base de données

#### Accès au shell MySQL

```sh
docker exec -it mariadb bash
```

et

```sh
mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD"
```

#### Backup de la base de données

```sh
mkdir -p data/db

source .env && docker exec $(docker-compose ps -q $MYSQL_DOCKER_CONTAINER_NAME) mysqldump --all-databases -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" > "data/db/db.sql"
```

ou

```sh
source .env && docker exec $(docker-compose ps -q $MYSQL_DOCKER_CONTAINER_NAME) mysqldump test -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" > "data/db/test.sql"
```

#### Restaurer la base de données

```sh
source .env && docker exec -i $(docker-compose ps -q $MYSQL_DOCKER_CONTAINER_NAME) mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" < "data/db/db.sql"
```

---

## Troubleshooting

###### MacOS : passer outre l’erreur « NET::ERR_CERT_INVALID »:

Il vous suffit de cliquer n’importe où sur la page puis de taper `thisisunsafe` et vous pourrez accéder au site.

*refs*: https://notamax.be/macos-passer-outre-lerreur-neterr_cert_invalid/

---

## Team

- Marc Galoyer <mgaloyer@uneak.fr> **Lead developper**
