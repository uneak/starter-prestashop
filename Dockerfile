ARG PS_DOCKER_VERSION
ARG PS_PHP_DOCKER_VERSION

FROM prestashop/prestashop:${PS_DOCKER_VERSION}-${PS_PHP_DOCKER_VERSION}-fpm as ps_prod_stage
LABEL maintainer="Marc Galoyer <mgaloyer@uneak.fr>"

COPY config/php/php.ini $PHP_INI_DIR/conf.d/php.ps-common.ini
COPY config/php/php.prod.ini $PHP_INI_DIR/conf.d/php.ps-prod.ini

ARG PS_FOLDER_ADMIN

WORKDIR /var/www/html

COPY ./public ./
# COPY ./src ./
RUN chmod -R 0755 ./

# RUN chmod -R +w \
#         ${PS_FOLDER_ADMIN}/autoupgrade \
#         app/config \
#         app/logs \
#         app/Resources/translations \
#         cache \
#         config \
#         download \
#         img \
#         log \
#         mails \
#         modules \
#         themes \
#         translations \
#         upload \
#         var


# COPY ./src/modules modules
# RUN chmod -R 0755 modules

# COPY ./src/themes themes
# RUN chmod -R 0755 modules

# COPY ./src/override override
# RUN chmod -R 0755 override



FROM prestashop/base:${PS_PHP_DOCKER_VERSION}-fpm as ps_dev_stage
LABEL maintainer="Marc Galoyer <mgaloyer@uneak.fr>"

ARG PS_NPM_DOCKER_VERSION
ARG PS_YARN_DOCKER_VERSION
ARG PS_NODE_DOCKER_VERSION
ARG PS_DOCKER_VERSION
ARG GROUP_ID
ARG USER_ID

ENV PS_VERSION ${PS_DOCKER_VERSION}

# To run files with the same group as your primary user
RUN groupmod -g $GROUP_ID www-data \
  && usermod -u $USER_ID -g $GROUP_ID www-data

RUN curl -sL https://deb.nodesource.com/setup_${PS_NODE_DOCKER_VERSION} | bash -
RUN apt install -y \
    apt-utils \
    mailutils \
    python2 \
    zip \
    nodejs \
    git

# install/configure npm
RUN npm cache clean -f \
    && npm install npm@${PS_NPM_DOCKER_VERSION} -g \
    && npm set strict-ssl false -g \
    && npm set progress false -g \
    && npm set loglevel error -g \
    && npm set registry https://registry.npmjs.org/ -g \
    && npm update

# add yarn
RUN npm install yarn@${PS_YARN_DOCKER_VERSION} -g

#
# Get PrestaShop 
#
RUN mkdir -p /tmp/r-data-ps/

# download by git
RUN git clone --depth=1 --branch=${PS_DOCKER_VERSION} https://github.com/PrestaShop/PrestaShop.git /tmp/r-data-ps/prestashop

# # download by zip package & refactor
# ADD https://github.com/PrestaShop/PrestaShop/archive/refs/tags/${PS_DOCKER_VERSION}.zip /tmp/prestashop.zip
# RUN mkdir -p /tmp/r-data-ps \
#     && unzip -q /tmp/prestashop.zip -d /tmp/r-data-ps/ \
#     && rm /tmp/prestashop.zip \
#     && mv /tmp/r-data-ps/PrestaShop-${PS_DOCKER_VERSION} /tmp/r-data-ps/prestashop

#
RUN (cd /tmp/r-data-ps && zip -r /tmp/prestashop.zip *)

# Extract
RUN mkdir -p /tmp/data-ps \
	&& unzip -q /tmp/prestashop.zip -d /tmp/data-ps/ \
	&& bash /tmp/ps-extractor.sh /tmp/data-ps \
	&& rm /tmp/prestashop.zip


# https://github.com/PrestaShop/php-ps-info
RUN git clone https://github.com/PrestaShop/php-ps-info.git /tmp/php-ps-info \
    && mv /tmp/php-ps-info/phppsinfo.php /var/www/html/phppsinfo.php \
    && rm -Rf /tmp/php-ps-info


# Apache configuration
RUN mkdir -p /var/www/.npm /var/www/.composer
RUN chown -R www-data:www-data \
    /var/www/.npm \
    /var/www/.composer \
    /var/www/html/ \
    /var/www/html/vendor \
    /var/www/html/var

# Get composer
# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN php -r "copy('https://getcomposer.org/installer', '/tmp/composer-setup.php');" && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer && rm -rf /tmp/composer-setup.php

#
# install hook scripts
#
RUN mkdir -p \
    /tmp/pre-install-scripts \
    /tmp/post-install-scripts \
    /tmp/init-scripts

# pre-install-scripts
COPY config/prestashop/scripts/composer-install.sh /tmp/pre-install-scripts/00-composer-install.sh
COPY config/prestashop/scripts/assets-install.sh /tmp/pre-install-scripts/01-assets-install.sh
COPY config/prestashop/scripts/assets-build.sh /tmp/pre-install-scripts/02-assets-build.sh

# post-install-scripts

#init-scripts
COPY config/prestashop/scripts/patch-image-type.sh /tmp/init-scripts/00-patch-image-type.sh

# set execute permission
RUN chmod +x \
    /tmp/pre-install-scripts/* \
    # /tmp/post-install-scripts/* \
    /tmp/init-scripts/*
#
