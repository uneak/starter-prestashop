#!/bin/sh

echo "\n* Build assets ...";

if [ ! -f ./$PS_FOLDER_ADMIN/themes/default/public/bundle.js ]; then
    echo "\n** Build admin default theme ..."
    (cd $PS_FOLDER_ADMIN/themes/default && yarn run build)
fi

if [ ! -f ./$PS_FOLDER_ADMIN/themes/new-theme/public/bundle.js ]; then
    echo "\n** Build admin new theme ..."
    (cd $PS_FOLDER_ADMIN/themes/new-theme && yarn run build)
fi

if [ ! -f ./themes/core.js ]; then
    echo "\n** Build core theme assets ..."
    (cd themes && yarn run build)
fi

if [ ! -d ./themes/classic/_dev/assets ]; then
    echo "\n** Build classic theme assets ..."
    (cd themes/classic/_dev && yarn run build)
fi
