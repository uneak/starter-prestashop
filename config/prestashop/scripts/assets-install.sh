#!/bin/sh

echo "\n* Install assets ...";

if [ ! -d ./$PS_FOLDER_ADMIN/themes/default/node_modules ]; then
    echo "\n** Install admin default theme ..."
    (cd $PS_FOLDER_ADMIN/themes/default && yarn install --force --no-lockfile --non-interactive --verbose > /root/.npm/_logs/admin-default-theme.log)
fi

if [ ! -d ./$PS_FOLDER_ADMIN/themes/new-theme/node_modules ]; then
    echo "\n** Install admin new theme ..."
    (cd $PS_FOLDER_ADMIN/themes/new-theme && yarn install --force --no-lockfile --non-interactive --verbose > /root/.npm/_logs/admin-new-theme.log)
fi

if [ ! -d ./themes/node_modules ]; then
    echo "\n** Install core theme assets ..."
    (cd themes && yarn install --force --no-lockfile --non-interactive --verbose > /root/.npm/_logs/theme.log)
fi

if [ ! -d ./themes/classic/_dev/node_modules ]; then
    echo "\n** Install classic theme assets ..."
    (cd themes/classic/_dev && yarn install --force --no-lockfile --non-interactive --verbose > /root/.npm/_logs/classic-theme.log)
fi


