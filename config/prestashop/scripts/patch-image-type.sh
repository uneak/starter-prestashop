#!/bin/sh

# https://github.com/PrestaShop/PrestaShop/issues/17725

# mysql $DB_NAME -h $DB_SERVER -P $DB_PORT -u $DB_USER -p$DB_PASSWD -e \
# "INSERT INTO `${DB_PREFIX}image_type` (`id_image_type`, `name`, `width`, `height`, `products`, `categories`, `manufacturers`, `suppliers`, `stores`)
# VALUES
# 	(10, 'large_banner', 960, 400, 0, 1, 0, 0, 0),
# 	(9, 'product_listing', 220, 220, 1, 1, 1, 1, 0),
# 	(8, 'category_default', 960, 350, 0, 1, 0, 0, 0),
# 	(7, 'home_default', 250, 250, 1, 0, 0, 0, 0),
# 	(6, 'large_default', 500, 500, 1, 0, 0, 0, 0),
# 	(5, 'medium_default', 300, 300, 1, 1, 1, 1, 0),
# 	(4, 'small_default', 125, 125, 1, 1, 1, 1, 0),
# 	(3, 'cart_default', 80, 80, 1, 0, 0, 0, 0);"
