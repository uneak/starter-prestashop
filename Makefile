# Makefile
# basé sur https://github.com/Idziak/docker-prestashop


include .env

#parametres de commande
# --all-databases
db=$(DB_NAME)
file=$(DB_NAME).sql
env=dev
#

cmd=--help
compose_file=-f docker-compose.yml -f docker-compose.$(env).yml

MYSQL_DUMPS_DIR=data/db
SSL_DIR=data/ssl
NGINX_LOG_DIR=data/nginx/log
NPM_LOG_DIR=data/npm/log
NGINX_CONF_DIR=data/nginx/conf.d
PUBLIC_DIR=public


help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  reset               	Supprime tout les fichiers du projet"
	@echo "  clean               	Supprime les informations relative au projet"
	@echo "  install               	Crée le projet"
	@echo "  build (env=prod)       Crée les containers"
	@echo "  start (env=prod)       Crée les containers et lance les services"
	@echo "  stop         			Arrète et nettoye les services"
	@echo "  ssl       		    	Génère le certificat SSL"
	@echo "  dump-db	          	Crée un backup de a base de donnée"
	@echo "  restore-db		       	Restore la base de donnée avec le backup"

console:
	docker-compose exec prestashop bin/console $(cmd)

composer:
	docker-compose exec prestashop composer $(cmd)

assets:
	docker-compose exec prestashop make assets

clean_data:
	@rm -Rf $(MYSQL_DUMPS_DIR)/*
	@rm -Rf $(NGINX_LOG_DIR)/*
	@rm -Rf $(NPM_LOG_DIR)/*
	@rm -Rf $(NGINX_CONF_DIR)/*

clean_ssl:
	@rm -Rf $(SSL_DIR)/$(DOMAIN)

clean_public:
	rm -Rf $(PUBLIC_DIR)

down:
	@docker-compose $(compose_file) down --remove-orphans

full_down:
	@docker-compose $(compose_file) down -v --rmi all --remove-orphans

debug:
	@docker-compose $(compose_file) config

build:
	@docker-compose $(compose_file) build --no-cache 

up: 
	@docker-compose $(compose_file) up -d

ssl:
	@mkdir -p $(SSL_DIR)/$(DOMAIN)
	@docker run --rm -v $(shell pwd)/$(SSL_DIR)/$(DOMAIN):/certificates -e "SERVER=$(DOMAIN)" -e "SUBJECT=/C=CA/ST=Canada/L=Canada/O=IT" jacoelho/generate-certificate

dump-db:
	@mkdir -p $(MYSQL_DUMPS_DIR)
	@docker exec $(shell docker-compose ps -q $(MYSQL_DOCKER_CONTAINER_NAME)) mysqldump $(db) -u"$(MYSQL_ROOT_USER)" -p"$(MYSQL_ROOT_PASSWORD)" > $(MYSQL_DUMPS_DIR)/$(file) 2>/dev/null
	# @make resetOwner

restore-db:
	@docker exec -i $(shell docker-compose ps -q $(MYSQL_DOCKER_CONTAINER_NAME)) mysql $(db) -u"$(MYSQL_ROOT_USER)" -p"$(MYSQL_ROOT_PASSWORD)" < $(MYSQL_DUMPS_DIR)/$(file) 2>/dev/null

resetOwner:
	@$(shell chown -Rf $(SUDO_USER):$(shell id -g -n $(SUDO_USER)) $(MYSQL_DUMPS_DIR) "$(shell pwd)/$(SSL_DIR)" "$(shell pwd)/${PUBLIC_DIR}" 2> /dev/null)

reset: full_down clean_data clean_ssl clean_public

clean: down clean_data

stop: down

start: build up

install: reset ssl start